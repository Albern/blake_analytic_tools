﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Analytics;

public class GameManager : MonoBehaviour
{
    public static int CurrentLevelNumber;

    [System.Flags]
    public enum Symbols
    {
        Waves =             1 << 0,
        Dot =               1 << 1,
        Square =            1 << 2,
        LargeDiamond =      1 << 3,
        SmallDiamonds =     1 << 4,
        Command =           1 << 5,
        Bomb =              1 << 6,
        Sun =               1 << 7,
        Bones =             1 << 8,
        Drop =              1 << 9,
        Face =              1 << 10,
        Hand =              1 << 11,
        Flag =              1 << 12,
        Disk =              1 << 13,
        Candle =            1 << 14,
        Wheel =             1 << 15
    }

    [System.Serializable]
    public struct LevelDescription
    {
        public int Rows, Columns;
        [EnumFlags]
        public Symbols UsedSymbols;
    }

    public GameObject TilePrefab;
    public float TileSpacing;

    public LevelDescription[] Levels = new LevelDescription[0];
    public Action HideTilesEvent;

    private Tile m_tileOne, m_tileTwo;
    private int m_cardsRemaining;

    int failedAttempts;
    int timeTaken;
    int gameTime;

    public GameObject helpMenu;

    private void Start()
    {
        LoadLevel( CurrentLevelNumber );
    }

    //loads the level and spawns the tiles
    private void LoadLevel( int levelNumber )
    {
        failedAttempts = 0;
        timeTaken = 0;
        gameTime = 0;
        InvokeRepeating("LevelTimer", 1, 1);
        InvokeRepeating("GameTimer", 1, 1);

        LevelDescription level = Levels[levelNumber % Levels.Length];

        List<Symbols> symbols = GetRequiredSymbols( level );

        //spawns the tiles in rows
        for ( int rowIndex = 0; rowIndex < level.Rows; ++rowIndex )
        {
            float yPosition = rowIndex * ( 1 + TileSpacing );
            for ( int colIndex = 0; colIndex < level.Columns; ++colIndex )
            {
                float xPosition = colIndex * ( 1 + TileSpacing );
                GameObject tileObject = Instantiate( TilePrefab, new Vector3( xPosition, yPosition, 0 ), Quaternion.identity, this.transform );
                int symbolIndex = UnityEngine.Random.Range( 0, symbols.Count );
                tileObject.GetComponentInChildren<Renderer>().material.mainTextureOffset = GetOffsetFromSymbol( symbols[symbolIndex] );
                tileObject.GetComponent<Tile>().Initialize( this, symbols[symbolIndex] );
                symbols.RemoveAt( symbolIndex );
            }
        }

        SetupCamera( level );
    }

    //checks to see what symbols need to be included on each tile
    private List<Symbols> GetRequiredSymbols( LevelDescription level )
    {
        List<Symbols> symbols = new List<Symbols>();
        int cardTotal = level.Rows * level.Columns;

        //makes sure there is an even number of cards and loads the selected symbols for the level
        {
            Array allSymbols = Enum.GetValues( typeof( Symbols ) );

            m_cardsRemaining = cardTotal;

            if ( cardTotal % 2 > 0 )
            {
                new ArgumentException( "There must be an even number of cards" );
            }

            foreach ( Symbols symbol in allSymbols )
            {
                if ( ( level.UsedSymbols & symbol ) > 0 )
                {
                    symbols.Add( symbol );
                }
            }
        }

        //makes sure there is at least one symbol selected as there aren't any leftover symbols that will never get put on cards
        {
            if ( symbols.Count == 0 )
            {
                new ArgumentException( "The level has no symbols set" );
            }
            if ( symbols.Count > cardTotal / 2 )
            {
                new ArgumentException( "There are too many symbols for the number of cards." );
            }
        }

        //goes through every symbol once before it adds duplicate symbols
        {
            int repeatCount = ( cardTotal / 2 ) - symbols.Count;
            if ( repeatCount > 0 )
            {
                List<Symbols> symbolsCopy = new List<Symbols>( symbols );
                List<Symbols> duplicateSymbols = new List<Symbols>();
                for ( int repeatIndex = 0; repeatIndex < repeatCount; ++repeatIndex )
                {
                    int randomIndex = UnityEngine.Random.Range( 0, symbolsCopy.Count );
                    duplicateSymbols.Add( symbolsCopy[randomIndex] );
                    symbolsCopy.RemoveAt( randomIndex );
                    if ( symbolsCopy.Count == 0 )
                    {
                        symbolsCopy.AddRange( symbols );
                    }
                }
                symbols.AddRange( duplicateSymbols );
            }
        }

        symbols.AddRange( symbols );

        return symbols;
    }

    //makes sure the symbol is centered on the tile
    private Vector2 GetOffsetFromSymbol( Symbols symbol )
    {
        Array symbols = Enum.GetValues( typeof( Symbols ) );
        for ( int symbolIndex = 0; symbolIndex < symbols.Length; ++symbolIndex )
        {
            if ( ( Symbols )symbols.GetValue( symbolIndex ) == symbol )
            {
                return new Vector2( symbolIndex % 4, symbolIndex / 4 ) / 4f;
            }
        }
        Debug.Log( "Failed to find symbol" );
        return Vector2.zero;
    }

    //sets the camera details to make sure it always covers the entire level no matter how big
    private void SetupCamera( LevelDescription level )
    {
        Camera.main.orthographicSize = ( level.Rows + ( level.Rows + 1 ) * TileSpacing ) / 2;
        Camera.main.transform.position = new Vector3( ( level.Columns * ( 1 + TileSpacing ) ) / 2, ( level.Rows * ( 1 + TileSpacing ) ) / 2, -10 );
    }

    //tells the selected tile to reveal itself
    public void TileSelected( Tile tile )
    {
        if ( m_tileOne == null )
        {
            m_tileOne = tile;
            m_tileOne.Reveal();
        }
        else if ( m_tileTwo == null )
        {
            m_tileTwo = tile;
            m_tileTwo.Reveal();
            //if the two symbols are the same, make a match
            if ( m_tileOne.Symbol == m_tileTwo.Symbol )
            {
                StartCoroutine( WaitForHide( true, 1f ) );

                CancelInvoke("LevelTimer");

                AnalyticsEvent.Custom("MatchMade", new Dictionary<string, object>
                {
                    { "time_taken", timeTaken }
                });

                timeTaken = 0;
                InvokeRepeating("LevelTimer", 1, 1);
            }
            //otherwise hide both tiles
            else
            {
                StartCoroutine( WaitForHide( false, 1f ) );
            }
        }
    }

    //moves on to the next level in the scene count
    private void LevelComplete()
    {
        ++CurrentLevelNumber;
        //if the current level is the last one, give a game over
        if ( CurrentLevelNumber > Levels.Length - 1 )
        {
            Debug.Log( "GameOver" );

            StopCoroutine("GameTimer");

            AnalyticsEvent.Custom("GameCompleted", new Dictionary<string, object>
            {
                { "game_time", gameTime }
            });

            gameTime = 0;
        }
        //otherwise load the next one
        else
        {
            SceneManager.LoadScene( SceneManager.GetActiveScene().buildIndex );
        }
    }

    private void LevelTimer()
    {
        timeTaken++;
    }

    private void GameTimer()
    {
        gameTime++;
    }

    public void HelpText()
    {
        if(helpMenu.activeSelf == false)
        {
            helpMenu.SetActive(true);

            AnalyticsEvent.Custom("HelpNeeded", new Dictionary<string, object>
            {
                { "help_accessed", gameTime }
            });
        }
        else
        {
            helpMenu.SetActive(false);
        }
    }

    //if another card is not picked in the set amount of time, it flips the tile back around. otherwise, it makes a comparison
    private IEnumerator WaitForHide( bool match, float time )
    {
        float timer = 0;
        while ( timer < time )
        {
            timer += Time.deltaTime;
            if ( timer >= time )
            {
                break;
            }
            yield return null;
        }
        //if a match is made, destroy the two matched tiles
        if ( match )
        {
            Destroy( m_tileOne.gameObject );
            Destroy( m_tileTwo.gameObject );
            m_cardsRemaining -= 2;
        }
        //otherwise, hide the tiles again and add onto the failed attempts
        else
        {
            m_tileOne.Hide();
            m_tileTwo.Hide();

            failedAttempts++;
        }
        m_tileOne = null;
        m_tileTwo = null;

        //if there are no tiles left on the board, complete the level
        if ( m_cardsRemaining == 0 )
        {
            CancelInvoke("LevelTimer");
            //sends the the statistics to unity analytics when a level is complete
            AnalyticsEvent.Custom("LevelComplete", new Dictionary<string, object>
            {
                { "failed_attempts", failedAttempts },
            });

            LevelComplete();
        }
    }

    public void OnApplicationQuit()
    {
        //tells analytics what level the player gave up on
        AnalyticsEvent.Custom("GameQuit", new Dictionary<string, object>
        {
            { "last_level", CurrentLevelNumber + 1 }
        });
    }
}